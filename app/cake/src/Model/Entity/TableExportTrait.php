<?php
namespace App\Model\Entity;

trait TableExportTrait
{
    public function serializeDisplay($variable, $display)
    {
        return empty($variable) ? null : $variable[$display];
    }

    public function serializeListFirst($list, $variable)
    {
        return empty($list) ? null : $list[0][$variable];
    }

    public function serializeListAll($list, $map)
    {
        return empty($list) ? null : implode('; ', array_map($map, $list));
    }
}
